import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';
import { Task } from './task.entity';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @Get('task')
  getTask() {
    return this.appService.getTasks();
  }

  @Get('mongosee')
  findAll(): Promise<Task[]> {
    return this.appService.findAll();
  }
}
