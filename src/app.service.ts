import { Inject, Injectable } from '@nestjs/common';
import { Db } from 'mongodb';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Task } from './task.entity';
import { log } from 'console';

@Injectable()
export class AppService {
  constructor(
    @Inject('MONGO') private dbMongo: Db,
    @InjectModel(Task.name) private taskModel: Model<Task>,
  ) {}

  getHello(): string {
    return 'Hello World!';
  }

  getTasks() {
    const taskCollection = this.dbMongo.collection('task');
    return taskCollection.find().toArray();
  }

  findAll(): Promise<Task[]> {
    log('vico');
    return this.taskModel.find().exec();
  }
}
