import { registerAs } from '@nestjs/config';

export default registerAs('config', () => {
  return {
    postgresUrl: process.env.DATABASE_URL,
    amqpUrl: process.env.AMQP_URL,
    cryptoJsKey: process.env.CRYPTO_JS_KEY,
    //cryptoJsKeyId: process.env.CRYPTO_JS_KEY_ID,
    mongo: {
      user: process.env.MONGO_INITDB_ROOT_USERNAME,
      pass: process.env.MONGO_INITDB_ROOT_PASSWORD,
      dbName: process.env.MONGO_DB,
      port: parseInt(process.env.MONGO_PORT, 10),
      host: process.env.MONGO_HOST,
      conection: process.env.MONGO_CONECTION,
    },
  };
});
