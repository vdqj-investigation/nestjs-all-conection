import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { Logger, ValidationPipe } from '@nestjs/common';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const logger = new Logger('Bootstrap');

  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true, // se llegan atributos que no estan definidos en el dtos, los ignora y continua
      forbidNonWhitelisted: true, // alerta de atributos que no esta definido en el esquema de los dtos
      transformOptions: { enableImplicitConversion: true }, // convierte string a number en @Query params
    }),
  );

  // DOCUMENT OF APIS
  const config = new DocumentBuilder()
    .setTitle('INVESTIGACION JWS')
    .setDescription('DOCUMENTACION - LICENCIA GPL')
    .setVersion('1.0')
    .addBearerAuth()
    .addTag('API')
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('/api/v1/docs', app, document, {
    swaggerOptions: {
      filter: true,
    },
  });

  // ENABLED ACCESS CORS ALL:
  app.enableCors();

  const port = process.env.PORT || 3000; // Puerto predeterminado 3000 o toma uno de las variables de entorno
  await app.listen(port);
  logger.log(`Servidor inicializado en: ${await app.getUrl()}`);
  logger.log(`Swagger: http://localhost:${await app.getUrl()}/api/v1/docs`);
}
bootstrap();
