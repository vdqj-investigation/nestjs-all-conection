import { Global, Module } from '@nestjs/common';
import { MongoClient } from 'mongodb';
import { MongooseModule } from '@nestjs/mongoose';
// import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigType } from '@nestjs/config';
// import { Client } from 'pg';

import config from '../../common/config/config';

@Global()
@Module({
  imports: [
    MongooseModule.forRoot('mongodb://localhost:27017', {
      user: 'victor',
      pass: 'victor',
      dbName: 'db_carrera',
    }),
    // MongooseModule.forRootAsync({
    //   useFactory: (configService: ConfigType<typeof config>) => {
    //     const { conection, user, pass, host, port, dbName } =
    //       configService.mongo;
    //     return {
    //       uri: `${conection}://${host}:${port}`,
    //       user,
    //       pass,
    //       dbName,
    //     };
    //   },
    //   inject: [config.KEY],
    // }),
  ],
  //   imports: [
  //     TypeOrmModule.forRootAsync({
  //       inject: [config.KEY],
  //       useFactory: (configEnv: ConfigType<typeof config>) => {
  //         return {
  //           type: 'postgres',
  //           url: configEnv.postgresUrl,
  //           ssl: {
  //             rejectUnauthorized: false,
  //           },
  //           synchronize: false, // para que la base de datos se sincronize conforme se creen los modelos
  //           autoLoadEntities: true, // sincronizar con las entidades creadas
  //         };
  //       },
  //     }),
  //   ],
  providers: [
    // {
    //   provide: 'PG',
    //   useFactory: (configEnv: ConfigType<typeof config>) => {
    //     const client = new Client({
    //       connectionString: configEnv.postgresUrl,
    //       ssl: {
    //         rejectUnauthorized: false,
    //       },
    //       idle_in_transaction_session_timeout: 0, // para que no caduque la conexion
    //       connectionTimeoutMillis: 0, // para que no caduque la conexion
    //     });
    //     client.connect();
    //     return client;
    //   },
    //   inject: [config.KEY],
    // },
    {
      provide: 'MONGO',
      useFactory: async (configService: ConfigType<typeof config>) => {
        const { conection, user, pass, host, port, dbName } =
          configService.mongo;
        const url = `${conection}://${user}:${pass}@${host}:${port}`;
        const client = new MongoClient(url);
        await client.connect();
        const database = client.db(`${dbName}`);
        return database;
      },
      inject: [config.KEY],
    },
  ],
  exports: ['MONGO', MongooseModule],
})
export class DatabaseModule {}
