import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { DatabaseModule } from './modules/database/database.module';

// const url = 'mongodb://victor:victor@localhost:27017';
// const client = new MongoClient(url);
// async function run() {
//   await client.connect();
//   const database = client.db('db_carrera');
//   const taskCollection = database.collection('task');
//   const task = await taskCollection.find().toArray();
//   log('tareas: ', task);
// }
// run();

import config from './common/config/config';
import { enviroments } from './common/config/enviroments';
import { MongooseModule } from '@nestjs/mongoose';
import { Task, TaskSchema } from './task.entity';

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: enviroments[process.env.NODE_ENV] || '.env',
      load: [config],
      isGlobal: true,
      // validationSchema: Joi.object({
      //   DATABASE_URL: Joi.string().required(),
      //   AMQP_URL: Joi.string().required(),
      //   CRYPTO_JS_KEY: Joi.string().required(),
      //   LOGIN_ENGRIPT: Joi.boolean().required(),
      //   API_SEGIP: Joi.string().required(),
      //   SEGIP_CONTRASTACION: Joi.string().required(),
      // }),
    }),
    DatabaseModule,
    MongooseModule.forFeature([
      {
        name: Task.name,
        schema: TaskSchema,
      },
    ]),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
